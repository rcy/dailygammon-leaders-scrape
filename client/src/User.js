import React from 'react'
import { useQuery } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import { LineChart, Line, CartesianGrid, YAxis, Legend } from 'recharts';
import './charts.css'

const USER_RATINGS = gql`
  query USER_RATINGS($username: String!) {
    userRatings(username: $username) {
      _id
      username
      rank
      experience
      rating
      createdAt
    }
  }
`

export default function User({ match }) {
  const { data, loading } = useQuery(USER_RATINGS, {
    variables: {
      username: match.params.username
    }
  })
  console.log({ data, loading })
  if (loading) return null;

  const latest = data.userRatings[0]

  const chartData = data.userRatings.reverse()

  const yAxisProps = {
    interval: "preserveStartEnd",
    domain: ['dataMin - 1', 'dataMax + 1']
  }

  return (
    <div>
      <h1>{match.params.username} - rating:{latest.rating} rank:{latest.rank} exp:{latest.experience} </h1>
      <LineChart width={600} height={200} data={chartData}>
        <CartesianGrid strokeDasharray="1 3" />
        <YAxis {...yAxisProps} yAxisId="rank" orientation="left" reversed />
        <YAxis {...yAxisProps} yAxisId="rating" orientation="right" />
        <Line yAxisId="rank" type="monotone" dataKey="rank" stroke="#82ca9d" strokeWidth={2} />
        <Line yAxisId="rating" type="monotone" dataKey="rating" stroke="#8884d8" strokeWidth={2} />
        <Legend />
      </LineChart>
    </div>    
  )
}
