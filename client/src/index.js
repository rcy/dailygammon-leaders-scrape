import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { ApolloProvider } from 'react-apollo';
import { ApolloProvider as ApolloProviderHooks } from "react-apollo-hooks";
import ApolloClient from "apollo-boost";

console.log(`connecting to ${process.env.REACT_APP_GRAPHQL_ENDPOINT}`)

const client = new ApolloClient({
  uri: process.env.REACT_APP_GRAPHQL_ENDPOINT
});

ReactDOM.render(
  <ApolloProvider client={client}>
    <ApolloProviderHooks client={client}>
      <App />
    </ApolloProviderHooks>
  </ApolloProvider>,
  document.getElementById('root')
);
