import React from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import User from './User'
import UserIndex from './UserIndex'

export default function App() {
  return (
    <Router>
      <div>
        <h1><Link to="/">DGStats</Link></h1>
        <Route path="/" exact component={UserIndex} />
        <Route path="/:username" component={User} />
      </div>
    </Router>
  )
}
