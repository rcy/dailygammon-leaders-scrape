import React, { useState } from 'react'
import gql from 'graphql-tag'
import { useQuery } from 'react-apollo-hooks'
import { Link } from 'react-router-dom'

const LATEST_RATINGS = gql`
  query LATEST_RATINGS($limit: Int, $skip: Int) {
    latestRatings(limit: $limit, skip: $skip) {
      _id
      username
      rank
      experience
      rating
      createdAt
    }
  }
`

export default function() {
  const [limit, setLimit] = useState(100)
  // const [skip, setSkip] = useState(0)

  const { data, loading, error } = useQuery(LATEST_RATINGS, {
    variables: {
      limit,
      skip: 0,
    }
  })
  if (loading) return null

  console.log({ loading, data, error })

  return (
    <div>
      {data.latestRatings.map(u => (
        <div key={u.username}>
          <Link to={`/${u.username}`}>
            {u.rank} - {u.username} {u.rating} {u.experience}
          </Link>
        </div>
      ))}
      <button onClick={() => setLimit(limit + 100)}>load more</button>
    </div>
  )
}
