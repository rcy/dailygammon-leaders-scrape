const MongoClient = require('mongodb')
const assert = require('assert')

const { MONGODB_URI } = process.env
assert(MONGODB_URI, '$MONGODB_URI not set')

module.exports = async function connect() {
  try {
    return await MongoClient.connect(MONGODB_URI, { useNewUrlParser: true })
  } catch(e) {
    console.error(e)
    process.exit(1)
  }
}
