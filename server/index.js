const { ApolloServer, gql } = require('apollo-server');
const connect = require('./mongo.js')

let Ratings

async function main() {
  const connection = await connect()
  Ratings = connection.db().collection('ratings')
}

main()

const typeDefs = gql`
  type Rating {
    _id: ID!
    rating: Float
    username: String
    rank: Int
    experience: Int
    createdAt: String
  }

  type Query {
    ratings: [Rating]
    latestRatings(limit: Int, skip: Int): [Rating]
    userRatings(username: String!): [Rating]
  }
`;

async function latestAt() {
  return (await Ratings.find().sort({ createdAt: -1 }).limit(1).toArray())[0].createdAt
}

const resolvers = {
  Query: {
    ratings() {
      return Ratings.find().limit(100).toArray()
    },
    async latestRatings(_, args = {}) {
      const createdAt = await latestAt()
      return Ratings.find({ createdAt })
                    .limit(args.limit || 100)
                    .skip(args.skip || 0)
                    .sort({ rank: 1 })
                    .toArray()
    },
    userRatings(_, { username }) {
      return Ratings.find({ username })
                    .sort({ createdAt: -1 })
                    .toArray()
    }
  },
};

const server = new ApolloServer({
  typeDefs,
  resolvers,
  introspection: true,
  playground: true,
});

server.listen({ port: process.env.PORT || 4000 }).then(({ url }) => {
  console.log(`🚀 Server ready at ${url}`);
});
